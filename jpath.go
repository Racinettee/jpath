package main

import (
    "encoding/json"
    "fmt"
    "io"
    "os"
    flags "github.com/jessevdk/go-flags"
    jpath "gitlab.com/Racinettee/jpath/lib"
)

func main() {
    var opts struct {
        Path string `short:"p" long:"path" description:"The object to seek" required:"true"`
        File string `short:"f" long:"file" description:"Optional parameter to specify a file to read from. If not present stdin is read from instead" optional:"true"`
    }
    _, err := flags.Parse(&opts)
    if err != nil {
        return
    }

    var reader io.Reader
    if opts.File != "" {
        jsonFile, err := os.Open(opts.File)
        if err != nil {
            fmt.Println(err)
            return
        }
        defer jsonFile.Close()
        reader = jsonFile
    } else {
        reader = os.Stdin
    }

    jsonPart := jpath.JsonPart(opts.Path, reader)

    json.NewEncoder(os.Stdout).Encode(jsonPart)
}
