# Example
`echo ' {"musician":{"billy idol":["white wedding", "rebel yell"]}}'|go run jpath.go --path="musician/billy idol/0"`
Or if that content was in a file:
`go run jpath.go --path="musician/billy idol/0" --file=musician.json`
