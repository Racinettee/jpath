package jpath

import (
    "encoding/json"
    "fmt"
    "io"
    "strings"
    "strconv"
)

func JsonPart(path string, reader io.Reader) (jsonPart interface{}) {
    var jsonInput interface{}

    json.NewDecoder(reader).Decode(&jsonInput)

    pathComponents := strings.Split(path, "/")

    jsonPart = jsonInput
    for _, component := range pathComponents {
        jArray, isArray := jsonPart.([]interface{})
        jMap, isMapping := jsonPart.(map[string]interface{})
        if isArray {
            i, err := strconv.Atoi(component)
            if err != nil {
                fmt.Println("Error in json path, expected number")
                return
            }
            jsonPart = jArray[i]
        } else if isMapping {
            jsonPart = jMap[component]
        } else {
            fmt.Println("Something else went wrong")
        }
    }
    return
}
